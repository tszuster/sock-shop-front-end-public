# Intro

PoC for CI/CD NG (Continuous Integration / Continuous Delivery or Deployment Next Generation).

The new solution should provide full automation from pull request to deployment on a Kubernetes Cluster.
Mentioned K8 cluster can be deployed on any Public Cloud provider as well as on on-premise.

Below documentation describes GitLab CI/CD which can be used as SaaS or self-hosted.
Initial tests were ran on GitLab server and worker in a Docker environment set up via docker compose tool and
microservice delivered to K8 distribution based on [minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)

This PoC shows how to deploy in an automatic way one microservice - front-end.

## Architecture diagram

![architecture](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/d5f0b6acc55e586da872ecd981fa07f1/GitLabCICDNG.png)

## Initial requirements
1. AWS EKS Cluster
2. GitLab account with trial gold subscription.

### AWS EKS Cluster
Please use [eksctl](https://github.com/weaveworks/eksctl/releases) (tested ver 0.15.0)

Below command will create AWS EKS cluster with managed worked nodes pool using t3.large with IAM policy allowing to:
* create load balancer (classic, network or application)
* access appmesh
* access ecr
* create ingress

Additionally dedicated kubectl config file will be created.

    eksctl create cluster \
    --name Cluster \
    --region eu-west-1 \
    --nodegroup-name standard-workers \
    --node-type t3.large \
    --nodes 3 \
    --nodes-min 1 \
    --nodes-max 4 \
    --ssh-access \
    --ssh-public-key ~/.ssh/id_rsa.pub \
    --managed \
    --version 1.15 \
    --auto-kubeconfig \
    --asg-access \
    --full-ecr-access \
    --appmesh-access \
    --alb-ingress-access

### GitLab SaaS
#### Create GitLab private git repository named sock-shop-front-end.

#### Create GitLab private container registry.

#### Runner / Worker
You could use GitLab worker from the shared pool, please keep in mind that your job has [limited hardware](https://forum.gitlab.com/t/what-are-resource-limits-for-ci-jobs-on-gitlab-com-ram-cpus/29821) to:
* 1 CPU
* 4GB of RAM
* 16GB disk space

Self-hosted workers needs to be used if the above parameters would be breached during CI/CD job.

##### AWS EC2 Spot
GitLab supports the AWS EC2 Spot instances via Docker Machine driver. To use Spot instances there need to be set up the [Runner Manager](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/#prepare-the-runner-manager-instance) which will spin up and down EC2 Spot instances.

Example code snippet for Runner Manager configuration:
```yaml

concurrent = 10
check_interval = 0

[[runners]]
  name = "gitlab-aws-autoscaler"
  url = "<URL of your GitLab instance>"
  token = "<Runner's token>"
  executor = "docker+machine"
  limit = 20
  [runners.docker]
    image = "alpine"
    privileged = true
    disable_cache = true
  [runners.cache]
    Type = "s3"
    Shared = true
    [runners.cache.s3]
      ServerAddress = "s3.amazonaws.com"
      AccessKey = "<your AWS Access Key ID>"
      SecretKey = "<your AWS Secret Access Key>"
      BucketName = "<the bucket where your cache should be kept>"
      BucketLocation = "us-east-1"
  [runners.machine]
    IdleCount = 1
    IdleTime = 1800
    MaxBuilds = 100
    OffPeakPeriods = [
      "* * 0-9,18-23 * * mon-fri *",
      "* * * * * sat,sun *"
    ]
    OffPeakIdleCount = 0
    OffPeakIdleTime = 1200
    MachineDriver = "amazonec2"
    MachineName = "gitlab-docker-machine-%s"
    MachineOptions = [
      "amazonec2-access-key=XXXX",
      "amazonec2-secret-key=XXXX",
      "amazonec2-region=us-central-1",
      "amazonec2-vpc-id=vpc-xxxxx",
      "amazonec2-subnet-id=subnet-xxxxx",
      "amazonec2-use-private-address=true",
      "amazonec2-tags=runner-manager-name,gitlab-aws-autoscaler,gitlab,true,gitlab-runner-autoscale,true",
      "amazonec2-security-group=docker-machine-scaler",
      "amazonec2-instance-type=m4.2xlarge",
      "amazonec2-request-spot-instance=true",
      "amazonec2-spot-price=0.03",
      "amazonec2-block-duration-minutes=60"
    ]
```
###### Caveats of AWS EC2 Spot instances
* one subnet
* one region
* one EC2 instance type

"...Running CI jobs on Spot instances may increase the failure rates because of the Spot instances pricing model. If the maximum Spot price you specify exceeds the current Spot price you will not get the capacity requested."

#### Create listed below CI/CD Variables, 
Go to `Settings -> CI/CD -> Variables`

| NO |      Key                  |  Val  | 
|----|---------------------------|------:|
| 1  | AWS_ACCESS_KEY_ID         ||
| 2  | AWS_SECRET_ACCESS_KEY     ||
| 3  | AWS_SESSION_TOKEN         ||
| 4  | DAST_WEBSITE              |Kubernetes LoadBalancer URL - AWS NLB, example aaaaaaaaaaaa04a8eaa4ced1148491b6-9f66542a4a60c9ce.elb.eu-west-1.amazonaws.com|
| 5  | DOCKER_REGISTRY           |tomaszszuster - public registry which hosts kubectl, helm and aws-iam-authenticator|
| 6  | IAM_USER_ROLE             |The name of IAM role assumed in Ginger sandbox account used to create AWS EKS cluster, example sandbox-xy1|
| 7  | K8_CLUSTER_ID             |The name of AWS EKS Cluster. Example Cluster|
| 8  | K8_CLUSTER_NAME           |The name of AWS EKS Cluster. Example Cluster.eu-west-1.eksctl.io|
| 8  | NAMESPACE                 |sock-shop|
| 8  | RUNNER                    |The name of your worker, I was using self hosted worker named udoo|
| 8  | SERVICE_NAME              |front-end|
    
#### Clone repository for sock-shop microservices
    
    git clone git@gitlab.com:tszuster/sock-shop-public.git
    git clone git@gitlab.com:tszuster/sock-shop-front-end-public.git

#### Deploy initial microservices to K8's
    
    kubectl apply -f sock-shop-public/deploy/kubernetes/complete-demo.yaml
    kubectl apply -f sock-shop-public/deploy/kubernetes/gitlab-admin-service-account.yaml
    
#### Create K8's integration in GitLab

Go to `Operations -> Kubernetes -> Add Kubernetes Cluster -> Add existing cluster`

Fill API URL, value can be obtained from:

    kubectl cluster-info                                                                                                                        
    Kubernetes master is running at https://F460721A91D25EC472197972119DA6E7.yl4.eu-west-1.eks.amazonaws.com

CA Certificate (<secret name> starts with default-token-, example default-token-wpwdr) 
    
    kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
   
Service Token

    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
    
Docker repository credentials, this credential will be used by K8's to pull docker image from private repository.

    kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=your_username --docker-password=your_password --docker-email=your_username
    
Confirm that integration has been created successfully by installing:
* Helm Tiller
* Prometheus
* Elastic Stack

To install please go to `Operations -> Kubernetes -> Applications`

After couple of minutes cluster metrics should be visible

![cluster_metrics](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/c8916c0fead3bde981bcd60fc2b86e12/k8_cluster_metrics.png)

#### Amend changes to sock-shop-front-end and push to your git repository

Try chaging **public/footer.html** file, chaging 4 lines down, starting from line number 57.

```html
                <p><strong>Weaveworks Ltd.</strong>
                    <br>32 – 38 Scrutton Street
                    <br>London
                    <br>EC2A 4RQ
                    <br>UK
                </p>

```

to

```html
                <p><strong>GitLab.</strong>
                    <br>The Internet
                    <br>Google Drive
                    <br>4321 123
                    <br>Earth
                </p>
```

**....and push changes to repository.**

Go to `CI/CD -> Pipelines`, from there you should see running pipeline with 5 stages:
![ci_cd_stages](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/a66dd9939f2852e6e29408a678b4db20/ci_cd_stages.png)
* Validate
* Build
* Test
    * code_quality
    * container_scanning
    * dependency_scanning
    * license_management
    * sast
* Deploy
* DAST
* Performance

#### Security & Compliance

After successful deployment there should be available data to review about code analysis.

**Security Dashboard**:

![Vulnerabilities](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/ade3c62b16871c8b6fad6211a52b46d8/vulnerabilities.png)

**Dependencies**:
![Dependencies](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/07a540243de9ae40b486d3aae612ecf8/dependencies.png)

**License Compliance**:
![License Compliance](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/cb3a796b69347f918424afcf45b7f8be/license_compliance.png)

*As we didn't deployed Ingress and WAF, then Threat Monitoring is not presenting any data.*

**Web application performance**
![page_performance](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/6e6b2e5881165361a75b5b95f290f8f9/page_speed.png)

##### Used tools in GitLab pipeline jobs:
* Code quality - [codeclimate](https://codeclimate.com)
* Dynamic Application Security Testing (DAST) - [OWASP ZAProxy](https://github.com/zaproxy/zaproxy)
* Container Scanning - [Clair](https://github.com/quay/clair) and [klar](https://github.com/optiopay/klar)
* Browser performance testing - [sitespeed.io](https://www.sitespeed.io/)
* Dependency scanning (owned by GitLab) - [gemnasium](https://docs.gitlab.com/ee/user/project/import/gemnasium.html)
* License scanning - [LicenseFinder](https://github.com/pivotal/LicenseFinder)
* Static Application Security Testing (SAST):
    * bandit, 
    * brakeman, 
    * gosec, 
    * spotbugs, 
    * flawfinder, 
    * phpcs-security-audit, k
    * security-code-scan, 
    * nodejs-scan, 
    * eslint, 
    * tslint, 
    * secrets, 
    * sobelow, 
    * pmd-apex, 
    * kubesec
### Check your deployed application

* Pods status

```bash
kubectl get po -n sock-shop                                                  
NAME                            READY   STATUS    RESTARTS   AGE
carts-7c6c688574-7nnwb          1/1     Running   0          2d23h
carts-db-c85755589-n576x        1/1     Running   0          2d23h
catalogue-6b9f764f97-8f7pz      1/1     Running   0          2d23h
catalogue-db-7cdf6bd55d-5vtj6   1/1     Running   0          2d23h
front-end-6dd57f47dd-bqmnw      1/1     Running   0          82m
front-end-6dd57f47dd-mlcg2      1/1     Running   0          81m
orders-5b8564dbc5-6cjcq         1/1     Running   0          2d23h
orders-db-7d74fc6c5f-hrb7v      1/1     Running   0          2d23h
payment-68d5774c57-zgzwj        1/1     Running   0          2d23h
queue-master-6b5c4b94f5-l982v   1/1     Running   0          2d23h
rabbitmq-785f864f59-ndmsg       1/1     Running   0          2d23h
shipping-558fc759cc-hjth5       1/1     Running   0          2d23h
user-575db54bfb-5mrq6           1/1     Running   0          2d23h
user-db-5cd49dfff6-4cwrx        1/1     Running   0          2d23h

```

* Service status

```bash
kubectl get svc -n sock-shop                                                
NAME           TYPE           CLUSTER-IP       EXTERNAL-IP                                                                     PORT(S)        AGE
carts          ClusterIP      10.100.45.146    <none>                                                                          80/TCP         2d23h
carts-db       ClusterIP      10.100.157.86    <none>                                                                          27017/TCP      2d23h
catalogue      ClusterIP      10.100.197.16    <none>                                                                          80/TCP         2d23h
catalogue-db   ClusterIP      10.100.76.52     <none>                                                                          3306/TCP       2d23h
front-end      LoadBalancer   10.100.6.187     aaaaaaaaaaaa04a8eaa4ced1148491b6-9f66542a4a60c9ce.elb.eu-west-1.amazonaws.com   80:30001/TCP   2d21h
orders         ClusterIP      10.100.210.162   <none>                                                                          80/TCP         2d23h
orders-db      ClusterIP      10.100.125.220   <none>                                                                          27017/TCP      2d23h
payment        ClusterIP      10.100.84.209    <none>                                                                          80/TCP         2d23h
queue-master   ClusterIP      10.100.241.130   <none>                                                                          80/TCP         2d23h
rabbitmq       ClusterIP      10.100.42.94     <none>                                                                          5672/TCP       2d23h
shipping       ClusterIP      10.100.2.17      <none>                                                                          80/TCP         2d23h
user           ClusterIP      10.100.28.57     <none>                                                                          80/TCP         2d23h
user-db        ClusterIP      10.100.206.158   <none>                                                                          27017/TCP      2d23h

```

* HTTP endpoint for deployed front-end application

```bash
kubectl get svc -n sock-shop front-end
NAME        TYPE           CLUSTER-IP     EXTERNAL-IP                                                                     PORT(S)        AGE
front-end   LoadBalancer   10.100.6.187   aaaaaaaaaaaa04a8eaa4ced1148491b6-9f66542a4a60c9ce.elb.eu-west-1.amazonaws.com   80:30001/TCP   2d21h

```

Use EXTERNAL-IP url to validate if front-end microservice is available from web browser. 

Validate if website footer presents amended data in `Where to find us` section
![front-end screenshot](https://gitlab.com/tszuster/sock-shop-front-end-public/-/wikis/uploads/53b1a093975cbb474b830d2b7b2a19b2/front-end-sock-shop.png)

In above example I've added changed footer with below data:

```html

                <p><strong>Here.</strong>
                    <br>At my home. v1
                    <br>Poznan
                    <br>60-179
                    <br>Poland
                </p>
```